import uuid
from datetime import datetime
from khayyam import JalaliDatetime
from lib import Root


class Item(Root):
    food_list = list()
    beverage_list = list()
    starter_list = list()
    item_count = 0
    item_list = list()
    item_del_list = list()
    item_type_dict = {
        'f': 'Food',
        'b': 'Beverage',
        's': 'Starter'
    }

    def __init__(self, name, item_type, price, append_to_list=True,
                 item_id=None, store_to_db=False, **kwargs):
        if item_id is None:
            self.item_id = self.item_count
            Item.item_count += 1
        else:
            self.item_id = item_id
        self.uuid = uuid.uuid4()
        self.name = name
        self.item_type = item_type.lower()
        self.price = price
        self.datetime = datetime.now()
        self.soft_delete = False
        if append_to_list:
            if self.item_type == 'f':
                Item.food_list.append(self)
            elif self.item_type == 's':
                Item.starter_list.append(self)
            elif self.item_type == 'b':
                Item.beverage_list.append(self)
            Item.item_list.append(self)
        super().__init__(store_to_db=store_to_db, **kwargs)

    @property
    def jalali_datetime(self):
        return JalaliDatetime(self.datetime)

    @classmethod
    def sample(cls, name='peperoni pizza', item_type='f', price=25000,
               store_to_db=False, append_to_list=True, item_id=None):
        return cls(name=name, item_type=item_type, price=price,
                   store_to_db=store_to_db, append_to_list=append_to_list,
                   item_id=item_id)

    @classmethod
    def init_from_dict(cls, objects_dict):
        max_item_count = 0
        for obj in objects_dict:
            item = cls.sample(store_to_db=False)
            max_item_count = max(max_item_count, (obj['item_id']+1))
            item.item_id = obj['item_id']
            item._id = obj['_id']
            item.name = obj['name']
            item.item_type = obj['item_type']
            item.price = obj['price']
            item.datetime = obj['datetime']
            item.soft_delete = obj['soft_delete']
            if item.item_type != 'f':
                cls.food_list.remove(item)
                if item.item_type == 's':
                    cls.starter_list.append(item)
                elif item.item_type == 'b':
                    cls.beverage_list.append(item)

        cls.item_count = max_item_count

    @classmethod
    def show_menu(cls):
        print("\n" + '#' * 20 + "\t\tRestaurant Menu\t\t" + '#' * 20)
        row = list()
        j = 0

        row.append('-'*14+' Food list '+'-'*15)
        j += 1
        for food in cls.food_list:
            row.append(f"{food.item_id:>3}. {food.name:25}  {food.price:>8}\t")
            j += 1

        row.append('-'*13+' Starter list '+'-'*13)
        j += 1
        for starter in cls.starter_list:
            row.append(f"{starter.item_id:>3}. {starter.name:25}  "
                       f"{starter.price:>8}")
            j += 1

        row.append('-'*12+' Beverage list '+'-'*13)
        j += 1
        for beverage in cls.beverage_list:
            row.append(f"{beverage.item_id:>3}. {beverage.name:25}  "
                       f"{beverage.price:>8}")
            j += 1

        for c in range(j):
            print(row[c])

    @classmethod
    def prompt(cls):
        from utils import input_number, get_and_check_id
        selected, item = get_and_check_id(cls, string="Item")
        how_many = input_number("How many")
        return {item: how_many}

    @classmethod
    def add(cls, store_to_db=True):
        from utils import create_item
        return create_item(store_to_db=store_to_db)

    @classmethod
    def show_all(cls):
        from utils import jalali_parser
        print(
            f"{'id':>5}_ {'name':20}  {'item_type':10}  {'price':>6}  "
            f"{'datetime':16}"
        )
        for item in cls.item_list:
            print(
                f"{item.item_id:>5}_ {item.name:20}  "
                f"{item.item_type_dict[item.item_type]:10}  "
                f"{item.price:>6}  "
                f"{jalali_parser(item.datetime):%Y-%m-%d %H:%M}"
            )

    @classmethod
    def edit(cls):
        from utils import get_and_check_id, edit_if_say_yes
        cls.show_all()
        selected, item = get_and_check_id(cls)
        item.name = edit_if_say_yes("name", item.name)
        item.item_type = edit_if_say_yes("item_type", item.item_type).lower()
        item.price = edit_if_say_yes("price", item.price, value_type="number")

        serialized_dict = item.serializer()
        _id = item.manager.update({'item_id': selected}, serialized_dict)
        return _id

    @classmethod
    def remove(cls):
        from utils import input_bool, get_and_check_id
        cls.show_all()
        selected, item = get_and_check_id(cls)
        are_you_sure = input_bool("Are you sure (y/n)", output="bool")
        if are_you_sure:
            cls.item_list.remove(item)
            cls.item_del_list.append(item)

            if item.item_type == 'f':
                Item.food_list.remove(item)
            elif item.item_type == 's':
                Item.starter_list.remove(item)
            elif item.item_type == 'b':
                Item.beverage_list.remove(item)

            Item.item_list.append(item)
            item.soft_delete = True
            _ = item.manager.update({'discount_id': selected},
                                    {'soft_delete': True})
            return item
