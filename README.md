# Restaurant POS Script
This script will help you to **manage a restaurant**. It has two main script, one for **restaurant manager** and an other for **restaurant staff**.

## Scripts details
Restaurant manager can add, edit, remove and see all:
- Items
- Tables
- Orders
- Discounts
- Supervisors

Restaurant staff can:
- Get an order or orders
- Checkout an order or orders
- Change I/O (in/out) or table for an order
- See all active orders details

## Requirements
You should have:
- Python 3.6+
- MongoDB 4.0+
- Sudo access (to start mongodb service)

## Quick Start
- Step 1 - create a virtual environment  (optional but **important**):
	- put project files on a sub directory named "src"
	- create a virtual environment in "restaurantpos-ver2.3.3" directory by:

    `virtualenv -p python3 venv`
    
	- run this command to activate the virtual environment:
	
    `source venv/bin/activate`

	- go to "src" sub directory by this command:

    `cd src/`

- Step 2 - install required packages
	- to install required packages run this command:
	
    `pip install -r requirements.txt`

- Step 3 - start mongodb service
	- run this command to start mongodb service:
	
    `sudo systemctl start mongod`

	- verify mongodb service status by this command:
	
    `systemctl status mongod`

- Step 4 - add at least one item and one table:
	- run main_supervisor.py:
	
    `python main_supervisor.py`

    or

    `python3 main_supervisor.py`

    if you haven't done step 1

	- by using "i" and then "a" option add at least one item. and then by pressing "b" you will back to main menu and then press "t" and then "a" to add at least one table. 
