from constants import SUP_OPTIONS_PROMPT_TEXT, SUP_OPTIONS
from utils import translate_sup_opt_to_cls, class_execute_method, \
    input_from_options, load_data

if __name__ == "__main__":
    load_data()
    working = True
    while working:
        selected_option = input(SUP_OPTIONS_PROMPT_TEXT).lower()
        if selected_option in SUP_OPTIONS.keys():
            if selected_option not in ['esc', 'r']:
                sub_menu_title = SUP_OPTIONS[selected_option]
                SelectedClass = translate_sup_opt_to_cls(selected_option)
                on_sub_menu = True
                while on_sub_menu:
                    selected_sub_option = input_from_options(
                        f"""   {'#' * 10} Manage {sub_menu_title}s
    a: add  {sub_menu_title}
    s: show all {sub_menu_title}s
    e: edit  {sub_menu_title}
    r: remove  {sub_menu_title}
    b: Back to main menu

    you wanna""", options="a/s/e/r/b"
                    )
                    selected_sub_option = selected_sub_option.lower()
                    if selected_sub_option == 'b':
                        on_sub_menu = False
                        break
                    _ = class_execute_method(
                        selected_class=SelectedClass,
                        selected_method=selected_sub_option
                    )

            elif selected_option == 'r':
                pass
            elif selected_option == 'esc':
                print(f"\n{'#' * 10} Bye! {'#' * 10}")
                working = False
        else:
            print("Invalid input. try again...")
