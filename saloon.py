import uuid
from lib import Root
from constants import YES_NO_DICT


class Table(Root):
    table_list = list()
    table_del_list = list()
    table_count = 0

    def __init__(self, capacity, number, append_to_list=True, table_id=None,
                 store_to_db=False, **kwargs):
        if table_id is None:
            self.table_id = Table.table_count
            Table.table_count += 1
        else:
            self.table_id = table_id
        self.uuid = uuid.uuid4()
        self.capacity = capacity
        self.number = number
        self.reserved = False
        self.is_available = True
        self.soft_delete = False
        if append_to_list:
            Table.table_list.append(self)
        super().__init__(store_to_db=store_to_db, **kwargs)

    @classmethod
    def sample(cls, capacity=6, number=1, store_to_db=False, table_id=None,
               append_to_list=True):
        return cls(capacity=capacity, number=number, store_to_db=store_to_db,
                   append_to_list=append_to_list, table_id=table_id)

    @classmethod
    def init_from_dict(cls, objects_dict):
        max_table_count = 0
        for obj in objects_dict:
            table = cls.sample(store_to_db=False)
            max_table_count = max(max_table_count, (obj['table_id']+1))
            table.table_id = obj['table_id']
            table._id = obj['_id']
            table.capacity = obj['capacity']
            table.number = obj['number']
            table.reserved = obj['reserved']
            table.is_available = obj['is_available']
            table.soft_delete = obj['soft_delete']

        cls.table_count = max_table_count

    @classmethod
    def add(cls, store_to_db=True):
        from utils import input_number
        print("Enter discount details: ")
        capacity = input_number("capacity")
        number = input_number("number")

        return cls(capacity=capacity, number=number, store_to_db=store_to_db)

    @classmethod
    def show_all(cls, show_available=False):
        print(
            f"{'id':>5}_ {'capacity':>9}  {'number':>7}  {'reserved':8}  "
            f"{'available':10}"
        )
        if show_available:
            for table in cls.table_list:
                if table.is_available:
                    print(
                        f"{table.table_id:>5}_ {table.capacity:>9}  "
                        f"{table.number:>7}  {YES_NO_DICT[table.reserved]:8}  "
                        f"{YES_NO_DICT[table.is_available]:10}"
                    )

        else:
            for table in cls.table_list:
                print(
                    f"{table.table_id:>5}_ {table.capacity:>9}  "
                    f"{table.number:>7}  {YES_NO_DICT[table.reserved]:8}  "
                    f"{YES_NO_DICT[table.is_available]:10}"
                )

    @classmethod
    def edit(cls):
        from utils import get_and_check_id, edit_if_say_yes
        cls.show_all()
        selected, table = get_and_check_id(cls)
        table.capacity = edit_if_say_yes("capacity", table.capacity,
                                         value_type="number")
        table.number = edit_if_say_yes("number", table.number,
                                       value_type="number")
        table.reserved = edit_if_say_yes("reserved (y/n)", table.reserved,
                                         value_type="bool")
        table.is_available = edit_if_say_yes("is_available (y/n)",
                                             table.is_available,
                                             value_type="bool")
        serialized_dict = table.serializer()
        _id = table.manager.update({'table_id': selected}, serialized_dict)
        return _id

    @classmethod
    def remove(cls):
        from utils import get_and_check_id, input_bool
        cls.show_all()
        selected, table = get_and_check_id(cls)
        are_you_sure = input_bool("Are you sure (y/n)", output="bool")
        if are_you_sure:
            cls.table_list.remove(table)
            cls.table_del_list.append(table)
            table.soft_delete = True
            _ = table.manager.update({'table_id': selected},
                                     {'soft_delete': True})
            return table
