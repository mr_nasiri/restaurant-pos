from constants import OPTIONS_PROMPT_TEXT
from order import Order
from saloon import Table
from utils import load_data, get_order, checkout_order, input_from_options, \
    edit_if_say_yes, get_and_check_id

if __name__ == "__main__":
    load_data()
    working = True
    while working:
        selected_option = input_from_options(OPTIONS_PROMPT_TEXT,
                                             options="o/c/i/s/r/e/esc").lower()
        if selected_option == 'o':
            ordering = True
            while ordering:
                items_dict = get_order()
                if items_dict is None:
                    ordering = False
                    break
                order = Order.add(items_dict=items_dict)
                wanna_order = input_from_options("wanna order (y/r/e)",
                                                 "y/r/e")
                if wanna_order == 'e':
                    ordering = False

        elif selected_option == 'c':
            checkout_orders = True
            while checkout_orders:
                order = checkout_order()
                if order is None:
                    checkout_orders = False
        elif selected_option == 'i':
            Order.show_all(show_all_order=False)
            selected, order = get_and_check_id(
                cls=Table, string="Which one do you want to edit:\nID"
            )
            order.in_out = edit_if_say_yes("in or out (I/O)", order.in_out,
                                           value_type="bool",
                                           true_false="I/O",
                                           output="string").upper()
            if order.in_out.lower() == 'i' and order.table is None:
                order.table = order.assign_table()
            elif order.in_out.lower() == 'o' and order.table is not None:
                order.table.is_available = True
                order.table.reserved = False
                order.table = None
            elif order.in_out.lower() == 'i' and len(
                    Table.manager.search(is_available=True)):
                order.table.is_available = True
                order.table.reserved = False
                order.table = order.assign_table()
                serialized_dict = order.serializer()
                _ = order.manager.update({'order_id': order.order_id},
                                         serialized_dict)
        elif selected_option == 's':
            Order.show_all(show_all_order=False)
        elif selected_option == 'esc':
            print(f"\n{'#' * 10} Bye! {'#' * 10}")
            working = False
