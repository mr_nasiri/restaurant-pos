import uuid
from khayyam import JalaliDatetime
from datetime import datetime
from lib import Root
from constants import YES_NO_DICT


class Payment(Root):
    payment_list = list()
    has_been_paid = list()
    payment_del_list = list()
    payment_count = 0

    def __init__(self, payment_type, price, is_paid=False, append_to_list=True,
                 payment_id=None, **kwargs):
        if payment_id is None:
            self.payment_id = Payment.payment_count
            Payment.payment_count += 1
        else:
            self.payment_id = payment_id
        self.uuid = uuid.uuid4()
        self.payment_type = payment_type.lower()
        self.is_paid = is_paid
        self.datetime = datetime.now()
        self.price = price
        self.soft_delete = False
        if append_to_list:
            Payment.payment_list.append(self)
        super().__init__(**kwargs)

        if self.is_paid:
            Payment.has_been_paid.append(self)

    @classmethod
    def sample(cls, payment_type='Cash', price=107000, is_paid=False,
               store_to_db=False, append_to_list=True):
        return cls(payment_type=payment_type, price=price, is_paid=is_paid,
                   store_to_db=store_to_db, append_to_list=append_to_list)

    @classmethod
    def init_from_dict(cls, objects_dict):
        max_payment_count = 0
        for obj in objects_dict:
            payment = cls.sample(store_to_db=False)
            max_payment_count = max(max_payment_count, (obj['payment_id']+1))
            payment.payment_id = obj['payment_id']
            payment._id = obj['_id']
            payment.payment_type = obj['payment_type']
            payment.is_paid = obj['is_paid']
            payment.datetime = obj['datetime']
            payment.price = obj['price']
            payment.soft_delete = obj['soft_delete']
            if payment.is_paid and payment not in cls.has_been_paid:
                cls.has_been_paid.append(payment)

        cls.payment_count = max_payment_count

    @property
    def jalali_datetime(self):
        return JalaliDatetime(self.datetime)

    @classmethod
    def paid_list(cls):
        return cls.has_been_paid

    def pay(self):
        self.is_paid = True
        serialized_dict = self.serializer()
        _ = self.manager.update({'payment_id': self.payment_id},
                                serialized_dict)
        Payment.has_been_paid.append(self)
        bill = Bill.manager.search(payment=self)
        Bill.unpaid_bills.remove(bill)
        Bill.paid_bills.append(bill)

    @classmethod
    def total_paid(cls):
        total = 0
        for payment in cls.has_been_paid:
            total += payment.price

        return total

    def ask_payment_type(self, ask_is_paid=False):
        from utils import input_bool
        payment_type = input_bool("Payment type (cash/pos)",
                                  true_false="cash/pos",
                                  output="string").lower()
        self.payment_type = payment_type
        serialized_dict = self.serializer()
        _ = self.manager.update({'payment_id': self.payment_id},
                                serialized_dict)

    @classmethod
    def add(cls, price, append_to_list=True, store_to_db=True,
            payment_id=None):
        payment_type = "_"
        return cls(payment_type=payment_type, price=price,
                   append_to_list=append_to_list, store_to_db=store_to_db,
                   payment_id=payment_id)

    @classmethod
    def show_all(cls):
        from utils import jalali_parser
        print(
            f"{'id':>6}_ {'type':6}  {'is_paid':7}  "
            f"{'datetime':16}  {'price':>9}"
        )
        for pay in cls.payment_list:
            print(
                f"{pay.payment_id:>6}_ {pay.payment_type:6}  "
                f"{YES_NO_DICT[pay.is_paid]:7}  "
                f"{jalali_parser(pay.datetime):%Y-%m-%d %H:%M}  {pay.price:>9}"
            )

    @classmethod
    def edit(cls, pay=None, price=None):
        from utils import edit_if_say_yes, get_and_check_id
        if pay is None:
            cls.show_all()
            selected, pay = get_and_check_id(cls)
        else:
            selected = pay.payment_id

        if price is None:
            price = pay.price

        pay.payment_type = edit_if_say_yes("payment_type (cash/pos)",
                                           pay.payment_type, value_type="bool",
                                           true_false="cash/pos",
                                           output="string")
        pay.price = price

        serialized_dict = pay.serializer()
        _id = pay.manager.update({'payment_id': selected}, serialized_dict)
        return _id

    @classmethod
    def remove(cls, pay=None):
        from utils import input_bool, get_and_check_id
        if pay is None:
            cls.show_all()
            selected, pay = get_and_check_id(cls)
            are_you_sure = input_bool("Are you sure (y/n)", output="bool")
        else:
            are_you_sure = True
            selected = pay.payment_id

        if are_you_sure:
            cls.payment_list.remove(pay)
            cls.payment_del_list.append(pay)
            pay.soft_delete = True
            _ = pay.manager.update({'payment_id': selected},
                                   {'soft_delete': True})
            return pay


class Bill(Root):
    bill_list = list()
    bill_del_list = list()
    unpaid_bills = list()
    paid_bills = list()
    bill_count = 0

    def __init__(self, total_price, payment, append_to_list=True, bill_id=None,
                 store_to_db=False, **kwargs):
        if bill_id is None:
            self.bill_id = Bill.bill_count
            Bill.bill_count += 1
        else:
            self.bill_id = bill_id
        self.uuid = uuid.uuid4()
        self.total_price = total_price
        self.payment = payment
        if payment != 1:
            self.payment_id = payment.payment_id
        else:
            self.payment_id = None
        self.soft_delete = False
        if append_to_list:
            Bill.unpaid_bills.append(self)
            Bill.bill_list.append(self)
        super().__init__(store_to_db=store_to_db, **kwargs)

    def serializer(self):
        serialized_dict = dict()
        for key in self.__dict__.keys():
            if isinstance(self.__dict__[key], (Payment, uuid.UUID)):
                pass
            else:
                serialized_dict[key] = self.__dict__[key]
        if serialized_dict.get('_id'):
            serialized_dict.pop('_id')
        if serialized_dict.get('index_in_n'):
            serialized_dict.pop('index_in_n')
        return serialized_dict

    @classmethod
    def sample(cls, total_price=107000, payment=None, store_to_db=False,
               append_to_list=True, bill_id=None):
        if payment is None:
            payment = Payment.sample()
        return cls(total_price=total_price, payment=payment, bill_id=bill_id,
                   store_to_db=store_to_db, append_to_list=append_to_list)

    @classmethod
    def init_from_dict(cls, objects_dict):
        max_bill_count = 0
        for obj in objects_dict:
            bill = cls.sample(store_to_db=False, payment=1)
            max_bill_count = max(max_bill_count, (obj['bill_id']+1))
            bill.bill_id = obj['bill_id']
            bill._id = obj['_id']
            bill.total_price = obj['total_price']
            bill.payment = Payment.manager.search(payment_id=obj['payment_id'])
            bill.payment_id = obj['payment_id']
            bill.soft_delete = obj['soft_delete']
            if bill.payment.is_paid:
                cls.unpaid_bills.remove(bill)
                cls.paid_bills.append(bill)

        cls.bill_count = max_bill_count

    @classmethod
    def show_unpaid(cls):
        return cls.unpaid_bills

    @classmethod
    def show_paid(cls):
        return cls.paid_bills

    @classmethod
    def add(cls, total=None, append_to_list=True, store_to_db=True,
            bill_id=None):
        from utils import input_number
        if total is None:
            print("Enter bill details: ")
            total = input_number("total_price")
        total_price = total
        payment = Payment.add(price=total_price, append_to_list=append_to_list,
                              store_to_db=store_to_db, payment_id=bill_id)
        return cls(total_price=total_price, payment=payment, bill_id=bill_id,
                   append_to_list=append_to_list, store_to_db=store_to_db)

    @classmethod
    def show_all(cls):
        from utils import jalali_parser
        print(
            f"{'id':>6}_ "
            f"{'datetime':16}  "
            f"{'price':>9}  {'pay_id':>6}  "
            f"{'pay_type':9}  {'is_paid':7}"
        )
        for bill in cls.bill_list:
            print(
                f"{bill.bill_id:>6}_ "
                f"{jalali_parser(bill.payment.datetime):%Y-%m-%d %H:%M}  "
                f"{bill.total_price:>9}  {bill.payment.payment_id:>6}  "
                f"{bill.payment.payment_type:9}  "
                f"{YES_NO_DICT[bill.payment.is_paid]:7}"
            )

    @classmethod
    def edit(cls, bill=None, total_price=None):
        from utils import get_and_check_id
        if bill is None:
            cls.show_all()
            selected, bill = get_and_check_id(cls)
        else:
            selected = bill.bill_id
        bill.total_price = total_price
        Payment.edit(pay=bill.payment, price=bill.total_price)
        serialized_dict = bill.serializer()
        _id = bill.manager.update({'bill_id': selected}, serialized_dict)
        return _id

    @classmethod
    def remove(cls, bill=None):
        from utils import input_bool, get_and_check_id
        if bill is None:
            cls.show_all()
            selected, dis = get_and_check_id(cls)
            are_you_sure = input_bool("Are you sure (y/n)", output="bool")
        else:
            are_you_sure = True
            selected = bill.bill_id

        if are_you_sure:
            cls.bill_list.remove(bill)
            cls.bill_del_list.append(bill)
            bill.soft_delete = True
            Payment.remove(pay=bill.payment)
            _ = bill.manager.update({'bill_id': selected},
                                    {'soft_delete': True})
            return bill
