from pymongo import MongoClient


RestaurantAgent = MongoClient()
RestaurantPOS = RestaurantAgent.RestaurantPOS


class Manager:
    def __init__(self, _class, _obj):
        self._class = _class
        self._obj = _obj

    @staticmethod
    def check_two_list_of_values(values, req_values):
        if len(values) == 0 or len(req_values) == 0 or len(values) != len(
                req_values):
            return False

        equal_values = True
        for i in range(len(values)):
            if values[i] != req_values[i]:
                equal_values = False
                break

        if equal_values:
            return True

    def search(self, **kwargs):
        if len(kwargs) == 0:
            return self._obj
        obj_list = list()
        for obj in self._obj:
            values = [getattr(obj, key) for key in kwargs.keys()]
            req_values = list(kwargs.values())
            if self.check_two_list_of_values(values, req_values):
                obj_list.append(obj)

        if len(obj_list) == 1:
            return obj_list[0]
        return obj_list

    def insert(self, data_dict):
        collection = RestaurantPOS[f"{self._class.__name__}".lower()]
        obj_id = collection.insert_one(
            data_dict
        ).inserted_id
        return str(obj_id)

    def update(self, search_dict, new_dict):
        collection = RestaurantPOS[f"{self._class.__name__}".lower()]
        result = collection.find_one_and_update(
            search_dict, {"$set": new_dict}
        )
        return result

    def delete(self, search_dict):
        collection = RestaurantPOS[f"{self._class.__name__}".lower()]
        result = collection.find_one_and_delete(
            search_dict
        )
        return result

    def find(self, search_dict={}):
        collection = RestaurantPOS[f"{self._class.__name__}".lower()]
        result = collection.find(
            search_dict
        )
        return result

    def init_from_dict(self, objects_dict):
        self._class.init_from_dict(objects_dict)


class Root:
    manager = None

    def __init__(self, store_to_db=True):
        self.set_manager()
        if store_to_db:
            serialized_dict = self.serializer()
            self._id = self.manager.insert(serialized_dict)

    def serializer(self):
        edited_dict = self.__dict__.copy()
        if edited_dict.get('_id'):
            edited_dict.pop('_id')
        if edited_dict.get('index_in_n'):
            edited_dict.pop('index_in_n')
        if edited_dict.get('uuid'):
            edited_dict.pop('uuid')
        return edited_dict

    def __iter__(self):
        self.index_in_n = 0
        return self

    def __next__(self):
        obj_list = getattr(type(self),
                           f"{type(self).__name__.lower()}_list")
        if self.index_in_n < len(obj_list):
            result = obj_list[self.index_in_n]
            self.index_in_n += 1
            return result
        else:
            raise StopIteration

    def set_manager(self):
        if type(self).manager is None:
            type(self).manager = Manager(type(self), self)
