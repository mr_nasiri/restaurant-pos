from lib import Root


class Supervisor(Root):
    supervisor_list = list()
    supervisor_del_list = list()
    supervisor_count = 0

    def __init__(self, username, password, phone_number, append_to_list=True,
                 supervisor_id=None, store_to_db=False, **kwargs):
        if supervisor_id is None:
            self.supervisor_id = Supervisor.supervisor_count
            Supervisor.supervisor_count += 1
        else:
            self.supervisor_id = supervisor_id
        self.username = username
        self.password = password
        self.phone_number = phone_number
        self.soft_delete = False
        if append_to_list:
            Supervisor.supervisor_list.append(self)
        super().__init__(store_to_db=store_to_db, **kwargs)

    @classmethod
    def sample(cls, username='ja4ari', password='123', store_to_db=False,
               phone_number=9377359595, append_to_list=True,
               supervisor_id=None):
        return cls(username=username, password=password,
                   phone_number=phone_number, store_to_db=store_to_db,
                   append_to_list=append_to_list, supervisor_id=supervisor_id)

    @classmethod
    def init_from_dict(cls, objects_dict):
        max_supervisor_count = 0
        for obj in objects_dict:
            supervisor = cls.sample(store_to_db=False)
            max_supervisor_count = max(max_supervisor_count,
                                       (obj['supervisor_id'] + 1))
            supervisor.supervisor_id = obj['supervisor_id']
            supervisor._id = obj['_id']
            supervisor.username = obj['username']
            supervisor.password = obj['password']
            supervisor.phone_number = obj['phone_number']
            supervisor.soft_delete = obj['soft_delete']

        cls.supervisor_count = max_supervisor_count

    @classmethod
    def add(cls, store_to_db=True):
        from utils import input_number
        print("Enter discount details: ")
        username = input("username: ")
        password = input("password: ")
        phone_number = input_number("phone_number")
        return cls(username=username, password=password,
                   phone_number=phone_number, store_to_db=store_to_db)

    @classmethod
    def show_all(cls):
        print(
            f"{'id':>4}_ {'username':16} "
            f"{'password':21}  {'phone_number':>13}"
        )
        for sup in cls.supervisor_list:
            print(
                f"{sup.supervisor_id:>4}_ {sup.username:16} "
                f"{sup.password:21}  {sup.phone_number:>13}"
            )

    @classmethod
    def edit(cls):
        from utils import edit_if_say_yes, get_and_check_id
        cls.show_all()
        selected, sup = get_and_check_id(cls)
        sup.password = edit_if_say_yes("password", sup.password)
        sup.phone_number = edit_if_say_yes("phone_number", sup.phone_number,
                                           value_type="number")

        serialized_dict = sup.serializer()
        _id = sup.manager.update({'supervisor_id': selected}, serialized_dict)
        return _id

    @classmethod
    def remove(cls):
        from utils import input_bool, get_and_check_id
        cls.show_all()
        selected, sup = get_and_check_id(cls)
        are_you_sure = input_bool("Are you sure (y/n)", output="bool")
        if are_you_sure:
            cls.supervisor_list.remove(sup)
            cls.supervisor_del_list.append(sup)
            sup.soft_delete = True
            _ = sup.manager.update({'supervisor_id': selected},
                                   {'soft_delete': True})
            return sup
