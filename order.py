import uuid
from datetime import datetime
from khayyam import JalaliDatetime
from finance import Bill
from lib import Root
from menu import Item
from saloon import Table


class Order(Root):
    order_list = list()
    order_del_list = list()
    un_paid_orders = list()
    order_count = 0
    IO_dict = {'I': 'in', 'O': 'out'}

    def __init__(self, item_dict, in_out, table, append_to_list=True,
                 order_id=None, **kwargs):
        if kwargs.get('store_to_db', None) is None:
            kwargs['store_to_db'] = True
        if order_id is None:
            self.order_id = Order.order_count
            Order.order_count += 1
        else:
            self.order_id = order_id
        self.uuid = uuid.uuid4()
        self.item_dict = item_dict
        self.in_out = in_out.upper()
        self.datetime = datetime.now()
        self.bill = None
        if item_dict != 1:
            self.set_bill(append_to_list=append_to_list, bill_id=order_id,
                          store_to_db=kwargs['store_to_db'])
            self.bill_id = self.bill.bill_id
        else:
            self.bill_id = None
        self.table = table
        if table is not None:
            if table != 1:
                self.table_id = self.table.table_id
            else:
                self.table_id = None
        else:
            self.table_id = None
        self.has_been_done = False
        self.soft_delete = False
        if append_to_list:
            Order.order_list.append(self)
            Order.un_paid_orders.append(self)
        super().__init__(**kwargs)

    def serializer(self):
        serialized_dict = dict()
        serialized_dict['item_dict'] = dict()
        for item in self.__dict__['item_dict'].keys():
            serialized_dict['item_dict'][str(item.item_id)] = \
                self.__dict__['item_dict'][item]
        for key in self.__dict__.keys():
            if isinstance(self.__dict__[key], (Bill, Table, uuid.UUID)) or \
                    key == 'item_dict' or key == '_id' or key == 'index_in_n':
                continue
            else:
                serialized_dict[key] = self.__dict__[key]
        return serialized_dict

    def calc_total_price(self):
        total_price = 0
        for item in self.item_dict.keys():
            total_price += item.price * self.item_dict[item]
        return total_price

    def calc_total_item(self):
        total_item = 0
        for item in self.item_dict.keys():
            total_item += self.item_dict[item]

        return total_item

    def set_bill(self, append_to_list=True, store_to_db=True, bill_id=None):
        total = self.calc_total_price()
        self.bill = Bill.add(total=total, append_to_list=append_to_list,
                             store_to_db=store_to_db, bill_id=bill_id)

    @staticmethod
    def assign_table():
        from utils import get_and_check_id
        assigning_table = True
        table = None
        while assigning_table:
            Table.show_all(show_available=True)
            table_id, table = get_and_check_id(
                cls=Table, string="Which table do wanna sit"
            )
            # table = Table.manager.search(table_id=table_id)
            if table.is_available and not table.reserved:
                table.is_available = False
                assigning_table = False
                serialized_dict = table.serializer()
                _ = table.manager.update(
                    {'table_id': table.table_id}, serialized_dict
                )

            else:
                print(f"table {table.number} is reserved or not available "
                      f"right now.\nPlease select an other table...")
        return table

    @staticmethod
    def change_table(previous_table):
        from utils import get_and_check_id, input_bool
        table = previous_table
        wanna_change_this = input_bool(
            f"Your previous table: {previous_table.number}\nDo you want to "
            f"change your table (y/n)", output="bool"
        )
        if wanna_change_this:
            assigning_table = True
            while assigning_table:
                Table.show_all(show_available=True)
                table_id, table = get_and_check_id(
                    cls=Table, string="Which table do wanna sit"
                )
                # table = Table.manager.search(table_id=table_id)
                if table.is_available and not table.reserved:
                    table.is_available = False
                    assigning_table = False
                    serialized_dict = table.serializer()
                    _ = table.manager.update(
                        {'table_id': table.table_id}, serialized_dict
                    )
                else:
                    print(f"table {table.number} is reserved or not available "
                          f"right now.\nPlease select an other table...")
        return table

    def checkout(self):
        if self.table is not None:
            self.table.is_available = True
            self.table.reserved = False
            serialized_dict = self.table.serializer()
            _ = self.table.manager.update({'table_id': self.table.table_id},
                                          serialized_dict)
        self.has_been_done = datetime.now()
        serialized_dict = self.serializer()
        _ = self.manager.update({'order_id': self.order_id},
                                serialized_dict)
        self.bill.payment.pay()

    @property
    def jalali_datetime(self):
        return JalaliDatetime(self.datetime)

    @classmethod
    def sample(cls, item_dict=None, in_out='I', store_to_db=False,
               table=None, append_to_list=True, order_id=None):
        if item_dict is None:
            item_dict = {Item.sample(append_to_list=append_to_list,
                                     item_id=order_id,
                                     store_to_db=store_to_db): 1}
        if table is None:
            table = Table.sample(append_to_list=append_to_list,
                                 table_id=order_id,
                                 store_to_db=store_to_db)
        return cls(item_dict=item_dict, in_out=in_out, table=table,
                   store_to_db=store_to_db, append_to_list=append_to_list,
                   order_id=order_id)

    @classmethod
    def init_from_dict(cls, objects_dict):
        max_order_count = 0
        for obj in objects_dict:
            order = cls.sample(store_to_db=False, item_dict=1, table=1)
            max_order_count = max(max_order_count, (obj['order_id']+1))
            order.order_id = obj['order_id']
            order._id = obj['_id']
            item_dict = obj['item_dict']
            order.item_dict = dict()
            for item in item_dict:
                order.item_dict[Item.manager.search(item_id=int(item))] = \
                    item_dict[item]

            order.in_out = obj['in_out']
            order.datetime = obj['datetime']
            order.has_been_done = obj['has_been_done']
            order.bill = Bill.manager.search(bill_id=obj['bill_id'])
            order.table_id = obj['table_id']
            if order.table_id is not None:
                order.table = Table.manager.search(table_id=order.table_id)
            else:
                order.table = None
            order.soft_delete = obj['soft_delete']
            if order.bill.payment.is_paid:
                cls.un_paid_orders.remove(order)

        cls.order_count = max_order_count

    @classmethod
    def add(cls, items_dict=None, store_to_db=True):
        from utils import input_bool, get_order
        print("Enter order details: ")
        if items_dict is None:
            items_dict = get_order(in_main=False)

        in_out = input_bool("in_out (I/O)", true_false="I/O", output="string")
        if in_out.lower() == 'i':
            table = cls.assign_table()
        else:
            table = None
        order = cls(item_dict=items_dict, in_out=in_out, table=table,
                    store_to_db=store_to_db)
        return order

    @classmethod
    def show_all(cls, show_all_order=True, order_list=None):
        from utils import jalali_parser
        from constants import YES_NO_DICT
        print(
            f"{'id':>6}_ {'total_item':>11}  "
            f"{'in_out':7}  {'table':>6}  "
            f"{'bill_id':>8}  {'price':>9}  {'pay_type':9}  "
            f"{'pay_datetime':16}  {'is_paid':7}"
        )
        if order_list is None:
            order_list = cls.order_list
        if not show_all_order:
            order_list = filter(lambda x: not x.has_been_done, order_list)
        for order in order_list:
            bill_id = order.bill.bill_id
            pay_datetime = jalali_parser(order.bill.payment.datetime)
            payment_type = order.bill.payment.payment_type
            is_paid = YES_NO_DICT[order.bill.payment.is_paid]

            table_number = order.table.number \
                if getattr(order, "table") else "_"

            total_price = str(order.calc_total_price())
            total_item = str(order.calc_total_item())

            print(
                f"{order.order_id:>6}_ {total_item:>11}  "
                f"{order.IO_dict[order.in_out]:7}  {table_number:>6}  "
                f"{bill_id:>8}  {total_price:>9}  "
                f"{payment_type:9}  ", end=""
            )

            if not isinstance(pay_datetime, JalaliDatetime):
                print(
                    f"{pay_datetime:16}  {is_paid:7}"
                )
            else:
                print(
                    f"{pay_datetime:%Y-%m-%d %H:%M}  {is_paid:7}"
                )

    @classmethod
    def edit(cls):
        from utils import get_and_check_id, edit_if_say_yes, edit_order
        cls.show_all()
        selected, order = get_and_check_id(cls)
        order.item_dict = edit_order(order.item_dict)
        order.bill.total_price = order.bill.payment.price = \
            order.calc_total_price()
        order.in_out = edit_if_say_yes("in_out", order.in_out,
                                       value_type="bool", true_false="I/O",
                                       output="string").upper()
        if order.in_out.lower() == 'i' and order.table is None:
            order.table = order.assign_table()
            order.table_id = order.table._id
        if order.in_out.lower() == 'o' and order.table is not None:
            order.table.is_available = True
            order.table.reserved = False
            order.table = None
            order.table_id = None

        order.datetime = edit_if_say_yes("datetime", order.datetime,
                                         value_type='datetime')

        total_price = order.calc_total_price()
        if order.bill is not None:
            Bill.edit(bill=order.bill, total_price=total_price)

        if order.table is not None:
            order.table = cls.change_table(order.table)
            order.table_id = order.table.table_id

        serialized_dict = order.serializer()
        _id = order.manager.update({'order_id': selected}, serialized_dict)
        return _id

    @classmethod
    def remove(cls):
        from utils import input_bool, get_and_check_id
        cls.show_all()
        selected, order = get_and_check_id(cls)
        are_you_sure = input_bool("Are you sure (y/n)", output="bool")
        if are_you_sure:
            cls.order_list.remove(order)
            cls.order_del_list.append(order)
            order.soft_delete = True
            _ = order.manager.update({'order_id': selected},
                                     {'soft_delete': True})
            order.table.is_available = True

            if order.bill is not None:
                order.bill.remove(bill=order.bill)
            return order
