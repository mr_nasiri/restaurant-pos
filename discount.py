from datetime import datetime
from lib import Root
from utils import input_number, edit_if_say_yes, input_date


class Discount(Root):
    discount_list = list()
    discount_count = 0
    discount_del_list = list()

    def __init__(self,  code, count, start_date, end_date, percentage,
                 minimum_order, maximum_order, append_to_list=True,
                 discount_id=None, store_to_db=False, **kwargs):
        if discount_id is None:
            self.discount_id = Discount.discount_count
            Discount.discount_count += 1
        else:
            self.discount_id = discount_id
        self.code = code
        self.count = count
        self.start_date = start_date
        self.end_date = end_date
        self.percentage = percentage
        self.minimum_order = minimum_order
        self.maximum_order = maximum_order
        self.soft_delete = False
        if append_to_list:
            Discount.discount_list.append(self)
        super().__init__(store_to_db=store_to_db, **kwargs)

    @classmethod
    def sample(cls, code="yalda_98", count="100", percentage=20,
               minimum_order=3, maximum_order=10, store_to_db=False,
               start_date=None, append_to_list=True, discount_id=None,
               end_date=None):
        if start_date is None:
            start_date = datetime.now()
        if end_date is None:
            end_date = datetime.now()
        return cls(
            code=code, count=count, percentage=percentage,
            start_date=start_date, end_date=end_date, store_to_db=store_to_db,
            minimum_order=minimum_order, maximum_order=maximum_order,
            append_to_list=append_to_list, discount_id=discount_id
        )

    @classmethod
    def add(cls, store_to_db=True):
        print("Enter discount details: ")
        code = input("code: ")
        count = input_number("count")
        start_date = input_date("start_date")
        end_date = input_date("end_date")
        percentage = input_number("percentage")
        minimum_order = input_number("minimum_order")
        maximum_order = input_number("maximum_order")

        return cls(
            code=code, count=count, percentage=percentage,
            start_date=start_date, end_date=end_date,
            minimum_order=minimum_order, maximum_order=maximum_order,
            store_to_db=store_to_db
        )

    @classmethod
    def show_all(cls):
        from utils import jalali_parser
        print(
            f"{'id':>4}_ {'code':13} {'count':>5}  "
            f"{'start_date':16}  "
            f"{'end_date':16}  "
            f"{'%':>4}"
            f"{'min_order':>10}{'max_order':>10}"
        )
        for dis in cls.discount_list:
            print(
                f"{dis.discount_id:>4}_ {dis.code:13} {dis.count:>5}  "
                f"{jalali_parser(dis.start_date):%Y-%m-%d %H:%M}  "
                f"{jalali_parser(dis.end_date):%Y-%m-%d %H:%M}  "
                f"{dis.percentage:>4}"
                f"{dis.minimum_order:>10}{dis.maximum_order:>10}"
            )

    @classmethod
    def edit(cls):
        from utils import get_and_check_id
        cls.show_all()
        selected, dis = get_and_check_id(cls)
        dis.code = edit_if_say_yes("code", dis.code)
        dis.count = edit_if_say_yes("count", dis.count, value_type="number")
        dis.start_date = edit_if_say_yes("start_date", dis.start_date,
                                         value_type='datetime')
        dis.end_date = edit_if_say_yes("end_date", dis.end_date,
                                       value_type='datetime')
        dis.percentage = edit_if_say_yes("percentage", dis.percentage,
                                         value_type="number")
        dis.minimum_order = edit_if_say_yes("minimum_order", dis.minimum_order,
                                            value_type="number")
        dis.maximum_order = edit_if_say_yes("maximum_order", dis.maximum_order,
                                            value_type="number")
        serialized_dict = dis.serializer()
        _id = dis.manager.update({'discount_id': selected}, serialized_dict)
        return _id

    @classmethod
    def remove(cls):
        from utils import input_bool, get_and_check_id
        cls.show_all()
        selected, dis = get_and_check_id(cls)
        are_you_sure = input_bool("Are you sure (y/n)", output="bool")
        if are_you_sure:
            cls.discount_list.remove(dis)
            cls.discount_del_list.append(dis)
            dis.soft_delete = True
            _ = dis.manager.update({'discount_id': selected},
                                   {'soft_delete': True})
            return dis

    @classmethod
    def init_from_dict(cls, objs_dict):
        max_dis_count = 0
        for obj in objs_dict:
            dis = cls.sample(store_to_db=False)
            max_dis_count = max(max_dis_count, (obj['discount_id']+1))
            dis.discount_id = obj['discount_id']
            dis._id = obj['_id']
            dis.code = obj['code']
            dis.count = obj['count']
            dis.start_date = obj['start_date']
            dis.end_date = obj['end_date']
            dis.percentage = obj['percentage']
            dis.minimum_order = obj['minimum_order']
            dis.maximum_order = obj['maximum_order']
            dis.soft_delete = obj['soft_delete']

        cls.discount_count = max_dis_count

