OPTIONS_PROMPT_TEXT = f"""{'#'*10} I want to:
o: get an Order or orders
c: Checkout an order or orders
i: change I/O or table for an order
s: Show all active order details
r: Reset current loop
e: Exit current loop
esc: Close program

{'#'*10} option"""

OPTIONS = {
    'o': "get an Order",
    'c': 'Checkout an order',
    'i': 'change I/O or table for an order',
    's': "show all active order details",
    'esc': "Close program"
}

YES_NO_DICT = {
    True: "yes",
    False: "no"
}

SUP_OPTIONS_PROMPT_TEXT = f"""{'#'*10} Restaurant POS Supervisor Options:
i: Items
t: Tables
o: Orders
d: Discount codes
u: Users
esc: Close program

{'#'*10} manage: """

SUP_OPTIONS = {
    'd': "Discount code",
    'i': "Item",
    'o': "Order",
    't': "Table",
    'u': "User",
    # 'r': "Restaurant",
    'esc': "Close program"
}

SUB_OPTIONS_PROMPT = f"""{'#'*10} Restaurant POS Options:
    o: Create new order
    r: Reset current loop
    e: Exist from current loop

    {'#'*10} option: """
