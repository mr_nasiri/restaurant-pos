from datetime import datetime
from finance import Bill, Payment
from menu import Item
from order import Order
from dateutil.parser import parse as date_parser
from khayyam import JalaliDatetime
from constants import YES_NO_DICT
from pymongo import MongoClient


RestaurantAgent = MongoClient()
RestaurantPOS = RestaurantAgent.RestaurantPOS


def get_and_check_id(cls, string="Which one do you want to edit:\nID"):
    selected = input_number(f"{string}")
    list_of_obj = getattr(cls, f"{cls.__name__}_list".lower())
    try:
        obj = list_of_obj[selected]
        # if selected not in cls.list_of_ids():
    except IndexError:
        print("Invalid input. this ID doesn't exist in the list"
              " or has been deleted.")
        return get_and_check_id(cls)
    return selected, obj


def create_sample_4test(store_to_db=False, append_to_list=False,
                        create_item=True, sample_id=-1):
    from discount import Discount
    from user import Supervisor
    discount_sample = Discount.sample(store_to_db=store_to_db,
                                      discount_id=sample_id,
                                      append_to_list=append_to_list)
    order_sample = Order.sample(store_to_db=store_to_db,
                                order_id=sample_id,
                                append_to_list=append_to_list)
    supervisor_sample = Supervisor.sample(store_to_db=store_to_db,
                                          supervisor_id=sample_id,
                                          append_to_list=append_to_list)
    item_sample = None
    if create_item:
        item_sample = Item.sample(store_to_db=store_to_db,
                                  item_id=sample_id,
                                  append_to_list=append_to_list)
    return discount_sample, order_sample, supervisor_sample, item_sample


def load_data(show_soft_delete=False, show_has_been_done=False):
    dis, order, sup, item = create_sample_4test(store_to_db=False,
                                                append_to_list=False)

    dis_dict = dis.manager.find({'soft_delete': False})
    dis.manager.init_from_dict(dis_dict)
    del dis

    item_dict = item.manager.find({'soft_delete': False})
    item.manager.init_from_dict(item_dict)
    del item

    pay_dict = order.bill.payment.manager.find({'soft_delete': False})
    order.bill.payment.manager.init_from_dict(pay_dict)
    del order.bill.payment

    bill_dict = order.bill.manager.find({'soft_delete': False})
    order.bill.manager.init_from_dict(bill_dict)
    del order.bill

    table_dict = order.table.manager.find({'soft_delete': False})
    order.table.manager.init_from_dict(table_dict)
    del order.table

    order_dict = order.manager.find(
        {'soft_delete': False, 'has_been_done': False}
    )
    order.manager.init_from_dict(order_dict)
    del order

    sup_dict = sup.manager.find({'soft_delete': False})
    sup.manager.init_from_dict(sup_dict)
    del sup


def jalali_parser(datetime):
    return JalaliDatetime(datetime)


def input_bool(string, true_false="y/n", output="bool"):
    true, false = true_false.split('/')
    value = input(f"{string}? ")
    try:
        if value.lower() not in [true.lower(), false.lower()]:
            raise ValueError
    except ValueError:
        print(
            f"Invalid input. \"{value}\" isn't one of "
            + f"({true_false}). try again..."
        )
        return input_bool(string, true_false, output)

    if output == "bool":
        if value == true:
            value = True
        else:
            value = False

    return value


def input_date(string, **kwargs):
    value = input(f"{string}: ")
    try:
        value = date_parser(value)
        if not isinstance(value, datetime):
            raise ValueError
    except ValueError:
        print(
            "Invalid input. \"{}\" isn't in corrent datetime".format(value)
            + "format. try again..."
        )
        return input_date(string)
    return value


def create_item(store_to_db=True):
    print("Enter item details: ")
    item_type = input("Item type (f: Food, s: Starter, b: Beverage): ")
    name = input("Item name: ")
    price = input_number("Price")
    return Item(name=name, item_type=item_type, price=price,
                store_to_db=store_to_db)


def input_number(string, **kwargs):
    value = input(f"{string}: ")
    try:
        value = int(value)
    except ValueError:
        print(
            "Invalid input. \"{}\" isn't a number. try again...".format(value))
        return input_number(string)
    return value


def input_from_options(string, options, obj_type=None, **kwargs):
    list_of_options = options.split('/')
    value = input(f"{string}: ").lower()
    try:

        try:
            if obj_type is not None:
                value = obj_type(value)
                return value

        except ValueError:
            pass

        if value in list_of_options:
            return value
        else:
            raise ValueError

    except ValueError:
        print(f"Invalid input. \"{value}\" isn't one of options ({options})."
              f" try again...")
        return input_from_options(string, options, obj_type, **kwargs)


def get_order(in_main=True):
    ordering = True
    order = {}
    if in_main:
        while ordering:
            Item.show_menu()
            new_item = Item.prompt()
            will_add_again = input_from_options(
                "Will you continue (y/n)? or you can (r/e)? ",
                options="y/n/r/e")
            if will_add_again == 'n':
                ordering = False
            elif will_add_again == 'r':
                order = {}
            elif will_add_again == 'e':
                return None
            order.update(new_item)
    else:
        while ordering:
            Item.show_menu()
            new_item = Item.prompt()
            will_add_again = input_from_options(
                "Will you continue (y/n)?",
                options="y/n")
            if will_add_again == 'n':
                ordering = False
            order.update(new_item)

    return order


def special_input_for_checkout_order_func():
    Order.show_all(show_all_order=False)
    options = ['r', 'e']
    obj = None
    value = input(
        "Which one do you want to checkout? or you can (r/e):\nID: "
    ).lower()
    try:

        try:
            value = int(value)
            list_of_obj = getattr(Order, "order_list")
            try:
                obj = list_of_obj[value]
                return value, obj
            except IndexError:
                print("Invalid input. this ID doesn't exist in the list"
                      " or has been deleted.")
                return special_input_for_checkout_order_func()
        except ValueError:
            if value in options:
                return value, obj
            else:
                raise ValueError

    except ValueError:
        print(f"Invalid input. \"{value}\" isn't one of options ({options})."
              f" try again...")
        return special_input_for_checkout_order_func()


def checkout_order():
    selected, order = special_input_for_checkout_order_func()
    if selected == 'r':
        return 'r'
    elif selected == 'e':
        return None
    # order = Order.manager.search(order_id=selected)
    order.bill.payment.ask_payment_type()
    order.show_all(order_list=[order])
    wanna_checkout = input_bool("Are you sure (y/n)")
    if wanna_checkout:
        order.checkout()
        return order

    return 'r'


def edit_order(order):
    for item in order.keys():
        wanna_change_this = input_bool(
            f"{item.name}: {order[item]}\nDo you want to edit this (y/n)",
            output="bool")
        if wanna_change_this:
            new_value = input_number(f"{item.name}")
            order[item] = new_value
    wanna_order_new_items = input_bool("Do you want to order new items "
                                       "(y/n)")
    if wanna_order_new_items:
        new_items = get_order()
        order.update(new_items)

    return order


def show_unpaid_bill():
    unpaid_bills = Bill.unpaid_bills
    for i, bill in enumerate(unpaid_bills):
        print(
            f"{i}. {bill.payment.payment_type}    {bill.payment.datetime}    "
            + f"{bill.uuid}\t\t{bill.total_price}"
        )


def pay_bill():
    row = input_number("Enter row number of bill that has been paid")
    bill = Bill.unpaid_bills[row]
    bill.payment.pay()


def get_finance_report():
    paid_list = Payment.paid_list()
    print(
        "#. price        pay_type  is_paid  datetime                      uuid"
    )
    for i, payment in enumerate(paid_list[-1:-11:-1]):
        print(
            f"{i}. {payment.price}\t\t{payment.payment_type}\t  "
            f"{YES_NO_DICT[payment.is_paid]}      {payment.datetime}   "
            f"{payment.uuid}"
        )
    print("{} total paid payments from total {} payments.".format(
        len(paid_list), len(Payment.payment_list)))


def edit_if_say_yes(string, value, value_type="string", **kwargs):
    input_as_wanted = input
    if value_type == "number":
        input_as_wanted = input_number
    elif value_type == "datetime":
        input_as_wanted = input_date
    elif value_type == "bool":
        input_as_wanted = input_bool

    wanna_change_this = input_bool(
        f"{string}: {value}\nDo you want to edit this (y/n)", output="bool")
    if wanna_change_this:
        if value_type != "string":
            value = input_as_wanted(f"{string}", **kwargs)
        else:
            value = input_as_wanted(f"{string}: ", **kwargs)
    return value


def translate_sup_opt_to_cls(selected_class):
    from discount import Discount
    from menu import Item
    from order import Order
    from saloon import Table
    from user import Supervisor
    sup_options_class = {
        'd': Discount,
        'i': Item,
        'o': Order,
        't': Table,
        'u': Supervisor,
    }
    return sup_options_class[selected_class]


def class_execute_method(selected_class, selected_method):
    if selected_method == 'a':
        execute_method = selected_class.add
    elif selected_method == 's':
        execute_method = selected_class.show_all
    elif selected_method == 'e':
        execute_method = selected_class.edit
    elif selected_method == 'r':
        execute_method = selected_class.remove

    return execute_method()
